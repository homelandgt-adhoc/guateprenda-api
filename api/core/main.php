<?php

if (strstr($_SERVER["PHP_SELF"], "/core/")) die ("You can't access this file directly...");
date_default_timezone_set("America/Guatemala");
require_once("config.php");
require_once("db.php");
require_once("functions.php");

session_start();
$arrGlobalNotFreedQueries = array();
$cfg = array();
$lang = array();
$arrAccesosPersonaToCheck = array();

///print_r($arrConfigSite);                        
$objResource = db_connect($arrConfigSite["db"]["host"],$arrConfigSite["db"]["database"],$arrConfigSite["db"]["user"],$arrConfigSite["db"]["password"],$arrConfigSite["db"]["port"]);
                   
//SI HUBO ERROR EN LA BASE DE DATOS, ENTONCES LO MANDO A LA PANTALLA DE CONSTRUCCION
if( !$objResource ) {
    header("Location: under_construction.html");
}
