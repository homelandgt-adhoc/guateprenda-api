<?php
require_once("../core/main.php");
$boolAnulada = false;
function fntGeneracionPDF($intTipo, $intFactura = 0, $boolPrueba = false){
	$strTexto = "";
	if ( $intTipo == 1 ){
		$strTexto = fntDrawFacturaJanama();
	}
	else if ( $intTipo == 2 ){
		$strTexto = fntDrawFacturaBrainerd();
	}
	else if ( $intTipo == 3 ){
		$strTexto = fntDrawReciboCompra();
	}
	else if ( $intTipo == 4 ){
		$strTexto = fntDrawFacturaVentaJanama();
	}
	else if ( $intTipo == 5 ){
		$strTexto = fntDrawFacturaVentaBrainerd();
	}
	else if ( $intTipo == 6 ){
		$strTexto = fntDrawFacturaExportacionBrainerd();
	}
	
	fntImpresionPDF($intTipo, $strTexto, $intFactura);
	
}

function fntDrawFacturaJanama(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">JANAMA HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Factura: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Cliente: [|-cliente-|]</td>
									  <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_gravada-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exenta</td>
									  <td style=\"text-align: right;\">L. [|-venta_exenta-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exonerada</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Sub Total</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_isv_15-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
									  <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntDrawFacturaBrainerd(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Factura: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Cliente: [|-cliente-|]</td>
									  <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_gravada-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exenta</td>
									  <td style=\"text-align: right;\">L. [|-venta_exenta-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exonerada</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Sub Total</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_isv_15-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
									  <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntDrawReciboCompra(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Boleta de compra: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Proveedor: [|-proveedor-|]</td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Domicilio del proveedor: [|-domicilio-|]</td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Telefono: [|-telefono-|]</td>
								  </tr>
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td width=\"50%\" style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; \">ARTICULOS USADOS</td>
								  </tr>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"40%\" style=\"text-align: left;\">Original: Contribuyente adquiriente</td>
									  <td width=\"60%\" style=\"text-align: left;\">Copia: Proveedor / Contabilidad</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Declaro que soy el propietario de los bienes descritos en este documento, por lo cual eximo a la Brainerd Honduras S.A. de toda responsabilidad.</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">__________________________________________</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">FIRMA DEL PROVEEDOR</td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntDrawFacturaVentaJanama(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">JANAMA HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Factura: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Cliente: [|-cliente-|]</td>
									  <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left;\">VENTA DE ARTICULOS USUADOS</td>
								  </tr>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_gravada-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exenta</td>
									  <td style=\"text-align: right;\">L. [|-venta_exenta-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exonerada</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Sub Total</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_isv_15-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
									  <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntDrawFacturaVentaBrainerd(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Factura: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Cliente: [|-cliente-|]</td>
									  <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left;\">ARTICULOS USADOS</td>
								  </tr>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_gravada-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exenta</td>
									  <td style=\"text-align: right;\">L. [|-venta_exenta-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exonerada</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Sub Total</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_isv_15-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
									  <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntDrawFacturaExportacionBrainerd(){
	
	$strTexto = "";
	
	$strTexto .= "<table width=\"100%\">
					  <tr>
						  <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Factura: [|-factura-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">CAI: [|-cai-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Rango autorizado: [|-rango_auto-|]</td>
					  </tr>
					  <tr>
						  <td style=\"text-align: left;\">Fecha limite de Emisi�n: [|-fecha_limite-|]</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Cliente: [|-cliente-|]</td>
									  <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">RTN / ID: [|-rtn_id-|]</td>
									  <td style=\"text-align: left;\">Fecha: [|-fecha-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Contrato: [|-contrato-|]</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. O/C Exenta</td>
									  <td style=\"text-align: left;\">No Registro de la SAG:</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">No. Registro Exonerado:</td>
									  <td style=\"text-align: left;\">&nbsp;</td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Tasa de Cambio: 24.57</td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Valor en US$: 411.07</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
									  <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
									  <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
								  </tr>
								  <<DESCRIPCION>>
								  <tr>
									  <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
									  <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_gravada-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Gravada 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exenta</td>
									  <td style=\"text-align: right;\">L. [|-venta_exenta-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Venta Exonerada</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Sub Total</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 15%</td>
									  <td style=\"text-align: right;\">L. [|-venta_isv_15-|]</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">ISV 18%</td>
									  <td style=\"text-align: right;\">L. 0.00</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\"><b>TOTAL</b></td>
									  <td style=\"text-align: right;\"><b>L. [|-total-|]</b></td>
								  </tr>
								  <tr>
									  <td colspan=\"2\" style=\"text-align: left;\">Son: [|-total_letras-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
					  </tr>
					  <tr>
						  <td>&nbsp;</td>
					  </tr>
					  <tr>
						  <td>
							  <table width=\"100%\">
								  <tr>
									  <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
									  <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
								  </tr>
								  <tr>
									  <td style=\"text-align: left;\">Atendido por:</td>
									  <td style=\"text-align: left;\">[|-nombre_empleado-|]</td>
								  </tr>
							  </table>
						  </td>
					  </tr>
				  </table>";
	
	return $strTexto;
	
}

function fntImpresionPDF($intTipo, $strTexto = "", $intFactura = 0){
	global $boolAnulada;
	$intTipo = intval($intTipo);
	$strTexto = !empty($strTexto) ? $strTexto : "";
	
	if ( $intTipo && !empty($strTexto) ){
		
		$strPDF = fntConvertInfo($intFactura, $strTexto);
		
		$strTitulo = "";
		if ( $intTipo == 1 ){
			$strTitulo = "FACTURA JANAMA";
		}
		else if ( $intTipo == 2 ){
			$strTitulo = "FACTURA BRAINERD";
		}
		else if ( $intTipo == 3 ){
			$strTitulo = "RECIBO DE COMPRA";
		}
		else if ( $intTipo == 4 ){
			$strTitulo = "FACTURA DE VENTA JANAMA";
		}
		else if ( $intTipo == 5 ){
			$strTitulo = "FACTURA DE VENTA BRAINERD";
		}
		else if ( $intTipo == 6 ){
			$strTitulo = "FACTURA DE EXPORTACI?n BRAINERD";
		}
		
		
		require_once("../core/tcpdf/tcpdf.php");
		//print $strPDF;
		
		$pdf = new TCPDF("P", "mm", "LETTER", false, 'ISO-8859-1', false);

		$pdf->SetTitle(utf8_encode($strTitulo));

		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);

		$pdf->SetFont('arial', '', 8);
		
		$pdf->AddPage();
		
		$pdf->writeHTML($strPDF, true, true, true, true, 'C');
		
		if( $boolAnulada == true ){
			$strFont = "helvetica";
			$strTexto = "A N U L A D A";
			$sinAltoMitad = ($pdf->getPageHeight()/8)*5;
			$sinAnchoMitad = ($pdf->getPageWidth()/16)*4;
			$pdf->SetAlpha(0.70);
			$intTotalPaginas = $pdf->getNumPages ();
			for($i =1; $i <= $intTotalPaginas; $i++){
				$pdf->setPage($i);
				$pdf->SetFont('helveticaB','',50);
				$pdf->SetTextColor(255,192,203);
				$pdf->StartTransform();
				$pdf->SetFont('helvetica');
				$pdf->Rotate(45,$sinAnchoMitad,$sinAltoMitad);
				$pdf->Text($sinAnchoMitad,$sinAltoMitad,$strTexto);
				$pdf->StopTransform();
			}
			$pdf->SetAlpha(1);
		}
		$pdf->Output();
		
	}
	
}

function fntConvertInfo($intFactura, $strPDFOriginal){
	global $boolAnulada;
	$intFactura = intval($intFactura);
	$strPDFOriginal = !empty($strPDFOriginal) ? $strPDFOriginal : "";
	
	$strPDF = "";
	if ( !empty($strPDFOriginal) ){
		
		$arrInfo = fntGetInfo($intFactura);		
		$strFactura = $arrInfo["factura"];
		$intCorre1 = $arrInfo["corre1"];
		$intCorre2 = $arrInfo["corre2"];
		$intCorre3 = $arrInfo["corre3"];
		$strCAI = $arrInfo["cai"];
		$strRango = $arrInfo["rango_autorizado"];
		$strFechaLimite = $arrInfo["fecha_limite"];
		$strCliente = $arrInfo["cliente"];
		$strRTNID = $arrInfo["rtn_id"];
		$strFecha = $arrInfo["fecha"];
		$strContrato = $arrInfo["contrato"];
		$sinVentaISV = $arrInfo["venta_gravada"];
		$sinVentaExenta = $arrInfo["venta_exenta"];
		$sinISV15 =  $arrInfo["venta_isv_15"];
		$sinTotal = $arrInfo["total"];
		$strTotal = $arrInfo["total_numeros"];
		$strEmpleado = $arrInfo["nombre_empleado"];
		$strProveedor = $arrInfo["proveedor"];
		$strDomicilio = $arrInfo["domicilio"];
		$strTelefono = $arrInfo["telefono"];
		
		$boolAnulada = (isset($arrInfo["anulada"]) && intval($arrInfo["anulada"]) == 1)?true:false;
		$arrDetalle = $arrInfo["detalle"];
		$strPDF = $strPDFOriginal;
		$strCorre1 = sprintf('%03d', $intCorre1);
		$strCorre2 = sprintf('%03d', $intCorre2);
		$strCorre3 = sprintf('%02d', $intCorre3);
		$strCorre4 = sprintf('%08d', $strFactura);
		$strFactura = "{$strCorre1}-{$strCorre2}-{$strCorre3}-{$strCorre4}";
		
		$strPDF = str_replace("[|-factura-|]", $strFactura, $strPDF);
		$strPDF = str_replace("[|-cai-|]", $strCAI, $strPDF);
		$strPDF = str_replace("[|-rango_auto-|]", $strRango, $strPDF);
		$strPDF = str_replace("[|-fecha_limite-|]", $strFechaLimite, $strPDF);
		$strPDF = str_replace("[|-cliente-|]", $strCliente, $strPDF);
		$strPDF = str_replace("[|-rtn_id-|]", $strRTNID, $strPDF);
		$strPDF = str_replace("[|-fecha-|]", $strFecha, $strPDF);
		$strPDF = str_replace("[|-contrato-|]", $strContrato, $strPDF);
		$strPDF = str_replace("[|-venta_gravada-|]", $sinVentaISV, $strPDF);		
		$strPDF = str_replace("[|-venta_exenta-|]", $sinVentaExenta, $strPDF);
		$strPDF = str_replace("[|-venta_isv_15-|]", $sinISV15, $strPDF);
		
		$strPDF = str_replace("[|-total-|]", $sinTotal, $strPDF);
		$strPDF = str_replace("[|-total_letras-|]", $strTotal, $strPDF);
		$strPDF = str_replace("[|-nombre_empleado-|]", $strEmpleado, $strPDF);
		$strPDF = str_replace("[|-proveedor-|]", $strProveedor, $strPDF);
		$strPDF = str_replace("[|-domicilio-|]", $strDomicilio, $strPDF);
		$strPDF = str_replace("[|-telefono-|]", $strTelefono, $strPDF);
		
		if ( is_array($arrDetalle) && ( count($arrDetalle) > 0 ) ){
			$strTextoReemplazar = '';
			reset($arrDetalle);
			foreach ( $arrDetalle as $key => $value ){				
				$strTextoReemplazar .= "<tr>
										   <td style=\"text-align: left;\">".$value["descripcion"]."</td>
										   <td style=\"text-align: left;\">".$value["cantidad"]."</td>
										   <td style=\"text-align: left;\">".$value["precio_unitario"]."</td>
										   <td style=\"text-align: left;\">".$value["subtotal"]."</td>
									   </tr>";
			}
						
			$strTextoBuscar = "<<DESCRIPCION>>";								
			$strPDF = str_replace($strTextoBuscar, $strTextoReemplazar, $strPDF);			
		}
		else{
			
			 $strTextoBuscar = "<tr>
									<td style=\"text-align: left;\">[|-descripcion-|]</td>
									<td style=\"text-align: left;\">[|-cantidad-|]</td>
									<td style=\"text-align: left;\">[|-precio_unitario-|]</td>
									<td style=\"text-align: left;\">[|-subtotal-|]</td>
								</tr>";
								
								
			 $strTextoReemplazar = "<tr>
										<td style=\"text-align: left;\">INTERESES</td>
										<td style=\"text-align: left;\">1</td>
										<td style=\"text-align: left;\">L. 51.87</td>
										<td style=\"text-align: left;\">L. 51.87</td>
									</tr>
									<tr>
										<td style=\"text-align: left;\">RECARGO</td>
										<td style=\"text-align: left;\">1</td>
										<td style=\"text-align: left;\">L. 3.50</td>
										<td style=\"text-align: left;\">L. 3.50</td>
									</tr>";
									
			 $strPDF = str_replace($strTextoBuscar, $strTextoReemplazar, $strPDF);			
		}
		
	}
	
	
	return $strPDF;
	
	
}

function fntGetInfo($intFactura){
	$intFactura = intval($intFactura);
	
	$arrInfo = array();
	
	$arrInfo["factura"] = "000-010-01-00000111";
	$arrInfo["anulada"] = 0;
	$arrInfo["corre1"] = 0;
	$arrInfo["corre2"] = 0;
	$arrInfo["corre3"] = 0;
	
	$arrInfo["cai"] = "4D4D4D-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90";
	$arrInfo["rango_autorizado"] = "000-010-01-00000101 al 000-010-01-00000501";
	$arrInfo["fecha_limite"] = "DD/MM/AAAA";
	$arrInfo["cliente"] = "XXXXXXXXXXXXXX";
	$arrInfo["rtn_id"] = "999999999900000";
	$arrInfo["fecha"] = "DD/MM/AAAA";
	$arrInfo["contrato"] = "";
	$arrInfo["venta_exenta"] = "0.00";
	$arrInfo["venta_gravada"] = "0.00";
	$arrInfo["venta_isv_15"] = "0.00";
		
	$arrInfo["total"] = "0.00";
	$arrInfo["total_numeros"] = "Cero Lempiras con 00/100";
	$arrInfo["nombre_empleado"] = "NOMBRE DE EMPLEADO";
	
	$arrInfo["proveedor"] = "";
	$arrInfo["domicilio"] = "";
	$arrInfo["telefono"] = "";
	
	$arrInfo["detalle"] = array();
	
	if ( $intFactura ){
		
		$strQuery = "SELECT factura.correlativo_serie, 

							factura.fecha,
							factura.usuario_transaccion,
							factura.anulada,
							factura.isv,
							serie.corr_primer_num, serie.corr_segundo_num, 
							serie.corr_tercer_num, serie.corr_cuarto_num, 
							serie.correlativo_inicial, serie.correlativo_final,
							serie.cai, serie.fecha_expiracion,
							factura.nombre as nombre_fac,
							factura.direccion as direccion_fac,
							cliente.cliente,
							factura.contrato,
							cliente.nombre,
							cliente.nit,
							cliente.direccion,
							NULL telefono 
					 FROM   factura
								LEFT JOIN cliente
									ON factura.cliente = cliente.cliente,
							serie
							
					 WHERE  factura.factura = '{$intFactura}'
					 AND    factura.serie = serie.serie";
		$arrContent = sqlGetValueFromKey($strQuery);
		$arrInfo["factura"] = !empty($arrContent["correlativo_serie"]) ? $arrContent["correlativo_serie"] : "000-010-01-00000111";
		$arrInfo["corre1"] = intval($arrContent["corr_primer_num"]);
		$arrInfo["corre2"] = intval($arrContent["corr_segundo_num"]);
		$arrInfo["corre3"] = intval($arrContent["corr_tercer_num"]);
		$arrInfo["anulada"] = intval($arrContent["anulada"]);		
		$arrInfo["cai"] = !empty($arrContent["cai"]) ? $arrContent["cai"] : "4D4D4D-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90";
		$arrInfo["rango_autorizado"] = !empty($arrContent["corr_primer_num"]) ? $arrContent["corr_primer_num"]."-".$arrContent["corr_segundo_num"]."-".$arrContent["corr_tercer_num"]."-".$arrContent["corr_cuarto_num"]."-".$arrContent["correlativo_inicial"]." al ".$arrContent["corr_primer_num"]."-".$arrContent["corr_segundo_num"]."-".$arrContent["corr_tercer_num"]."-".$arrContent["corr_cuarto_num"]."-".$arrContent["correlativo_final"] : "000-010-01-00000101 al 000-010-01-00000501";
		$arrInfo["fecha_limite"] = !empty($arrContent["fecha_expiracion"]) ? getShowDate($arrContent["fecha_expiracion"], false) : "DD/MM/AAAA";
		
		if(  isset($arrContent["cliente"]) && intval($arrContent["cliente"]) > 0 ){
			$arrInfo["rtn_id"] = !empty($arrContent["nit"]) ? $arrContent["nit"] : "";
			$arrInfo["cliente"] = !empty($arrContent["nombre"]) ? $arrContent["nombre"] : "";
			$arrInfo["proveedor"] = !empty($arrContent["nombre"]) ? $arrContent["nombre"] : "";
			$arrInfo["domicilio"] =  !empty($arrContent["direccion"]) ? $arrContent["direccion"] : "";			
		}
		else{
			$arrInfo["rtn_id"] =  "CF";
			$arrInfo["cliente"] = !empty($arrContent["nombre_fac"]) ? $arrContent["nombre_fac"] : "";
			$arrInfo["proveedor"] = !empty($arrContent["nombre_fac"]) ? $arrContent["nombre_fac"] : "";
			$arrInfo["domicilio"] =  !empty($arrContent["direccion_fac"]) ? $arrContent["direccion_fac"] : "";
		}
		$arrInfo["telefono"] =  !empty($arrContent["telefono"]) ? $arrContent["telefono"] : "";

		$arrInfo["fecha"] = !empty($arrContent["fecha"]) ? getShowDate($arrContent["fecha"], false) : "DD/MM/AAAA";
		$arrInfo["contrato"] =  !empty($arrContent["contrato"]) ? $arrContent["contrato"] : "";
		$arrInfo["nombre_empleado"] = !empty($arrContent["usuario_transaccion"]) ? $arrContent["usuario_transaccion"] : "NOMBRE DE EMPLEADO";
		
		$sinTotal = 0;
		$sinTotalGravada = 0;
		$sinTotal_ISV =  0;
		$sinTotalExento = 0;
		$strQuery = "SELECT factura_detalle, descripcion, 
							cantidad, 
							precio_unitario, 
							subtotal,
							isv
					 FROM   factura_detalle
					 WHERE  factura = '{$intFactura}'
					 ORDER  BY factura_detalle";
		$qTMP = db_query($strQuery);
		while ( $rTMP = db_fetch_assoc($qTMP) ){
			$arrDetalle[$rTMP["factura_detalle"]]["factura_detalle"] = $rTMP["factura_detalle"];
			$arrDetalle[$rTMP["factura_detalle"]]["descripcion"] = $rTMP["descripcion"];
			$arrDetalle[$rTMP["factura_detalle"]]["cantidad"] = number_format($rTMP["cantidad"],2);
			$arrDetalle[$rTMP["factura_detalle"]]["precio_unitario"] = number_format($rTMP["precio_unitario"],2);
			$sinISV = floatval($rTMP["isv"]);
			$sinSubTotal = floatval($rTMP["subtotal"]);

			$arrDetalle[$rTMP["factura_detalle"]]["subtotal"] = number_format( $sinSubTotal ,2);
			$arrDetalle[$rTMP["factura_detalle"]]["isv"] =  number_format($sinISV,2);
			$sinTotal += $sinSubTotal; 
			if( $sinISV > 0){
				$sinTotal_ISV += $sinISV;
				$sinTotalGravada += ( $sinSubTotal - $sinISV );
			}
			else{
				$sinTotalExento += $sinSubTotal;	
			}			
		}
		db_free_result($qTMP);
			
		$arrInfo["venta_isv_15"] =  floatval($sinTotal_ISV) ? number_format( $sinTotal_ISV,2 ) : "0.00";
		$arrInfo["venta_gravada"] =  floatval($sinTotalGravada) ? number_format( $sinTotalGravada,2 ) : "0.00"; 
		$arrInfo["venta_exenta"] = floatval($sinTotalExento) ? number_format( $sinTotalExento,2 ) : "0.00";
		
		$arrInfo["total"] = floatval($sinTotal) ? number_format( $sinTotal,2 ) : "0.00";
		$arrInfo["total_numeros"] = floatval($sinTotal) ?  GetPrecioEnLetras($sinTotal, "Lempiras") : "Cero Lempiras con 00/100";
		
		$arrInfo["detalle"] = $arrDetalle;
		
	}
	
	return $arrInfo;
	
}

function getShowDate($date, $showtime = true, $boolShowDateText = false) {

	$strDivChar = "/";
	$strDay = substr($date, 8, 2);
	$strMonth = substr($date, 5, 2);
	$strYear = substr($date, 0, 4);
	
	$strDate = $strDay . $strDivChar . $strMonth . $strDivChar . $strYear;
	if ($showtime) $strDate .= " " . substr($date, 11, 5);
	elseif( $boolShowDateText ) $strDate = $strDay ." de ".Get_Month_Text($strMonth)." de ". $strYear;

	return $strDate;
}

function getShowDateText($date, $showtime = true, $boolShowDateText = false) {

	$strDivChar = "/";
	$strDay = substr($date, 0, 2);
	$strMonth = substr($date, 3, 2);
	$strYear = substr($date, 6, 6);
	
	$strDate = $strDay . $strDivChar . $strMonth . $strDivChar . $strYear;
	if ($showtime) $strDate .= " " . substr($date, 11, 5);
	elseif( $boolShowDateText ) $strDate = $strDay ." de ".Get_Month_Text($strMonth)." de ". $strYear;

	return $strDate;
}


/**
 * @return string
 * @param integer $intNumber El n?mero a convertir, puede tener 2 decimales tambi?n
 * @desc Devuelve un numero en letras (estandar para dinero)
*/
function GetPrecioEnLetras($intNumber, $strMonedaName = "") {
	$arrParts = explode(".",$intNumber);
	$arrParts["int"] = $arrParts[0];
	if (!isset($arrParts[1])) {
		$arrParts["dec"] = "00";
	}
	else {
		$intDecLen = strlen($arrParts[1]);
		if ($intDecLen==1) {
			$arrParts["dec"]=$arrParts[1]."0";
		}
		else {
			$arrParts["dec"]=$arrParts[1];
		}
	}
	unset($arrParts[0]);
	unset($arrParts[1]);

	$strReturn = GetNumeroEnLetras($intNumber);
	if ($strMonedaName)
		$strReturn.= " ".$strMonedaName." con {$arrParts["dec"]}/100";
	else
		$strReturn.=" con {$arrParts["dec"]}/100";

	return trim($strReturn);
}

/**
 * @return string
 * @param integer $intNumber El n�mero a convertir, puede tener 2 decimales tambi?n
 * @param array $arrStrings (NO USAR ESTE PARAMETRO, ES PARA USO DE LA RECURSION EN LA FUNCION)
 * @desc Devuelve un numero en letras (estandar para dinero)
*/
function GetNumeroEnLetras($intNumber, $arrStrings = false) {
	$boolFirst = false;
	if (!$arrStrings) {
		$boolFirst = true;
		$intNumber = round($intNumber,2)."";
		$arrStrings = array();
		$arrStrings[0][1] = "uno";
		$arrStrings[0][2] = "dos ";
		$arrStrings[0][3] = "tres ";
		$arrStrings[0][4] = "cuatro ";
		$arrStrings[0][5] = "cinco ";
		$arrStrings[0][6] = "seis ";
		$arrStrings[0][7] = "siete ";
		$arrStrings[0][8] = "ocho ";
		$arrStrings[0][9] = "nueve ";

		$arrStrings[1][1] = "dieci";
		$arrStrings[1][2] = "veinti";
		$arrStrings[1][3] = "treinta y ";
		$arrStrings[1][4] = "cuarenta y ";
		$arrStrings[1][5] = "cincuenta y ";
		$arrStrings[1][6] = "sesenta y ";
		$arrStrings[1][7] = "setenta y ";
		$arrStrings[1][8] = "ochenta y ";
		$arrStrings[1][9] = "noventa y ";

		$arrStrings[2][1] = "ciento ";
		$arrStrings[2][2] = "doscientos ";
		$arrStrings[2][3] = "trescientos ";
		$arrStrings[2][4] = "cuatrocientos ";
		$arrStrings[2][5] = "quinientos ";
		$arrStrings[2][6] = "seiscientos ";
		$arrStrings[2][7] = "setecientos ";
		$arrStrings[2][8] = "ochocientos ";
		$arrStrings[2][9] = "novecientos ";

		$arrStrings[3][1] = "mil ";
	}

	if ($intNumber == 0) return "cero";

	$arrParts = explode(".",$intNumber);
	$arrParts["int"] = $arrParts[0];
	if (!isset($arrParts[1])) {
		$arrParts["dec"] = "00";
	}
	else {
		$intDecLen = strlen($arrParts[1]);
		if ($intDecLen==1) {
			$arrParts["dec"]=$arrParts[1]."0";
		}
		else {
			$arrParts["dec"]=$arrParts[1];
		}
	}
	unset($arrParts[0]);
	unset($arrParts[1]);

	$strTMP = $arrParts["int"];
	$arrParts["int"] = array();
	for ($i=strlen($strTMP);$i>0;$i--) {
		$arrParts["int"][$i-1] = substr($strTMP,strlen($strTMP) - $i,1);
	}
	ksort($arrParts["int"]);

	$strReturn = "";
	foreach($arrParts["int"] as $arrThis["key"] => $arrThis["value"] ){
		if( $arrThis["key"] < 3 ){
			$strTMP = "";
			if ($arrThis["key"]==1 && $arrThis["value"]==1 && $arrParts["int"][0] < 6) {
				switch ($arrParts["int"][0]) {
					case 0:
					$strReturn = "diez";
					break;
					case 1:
					$strReturn = "once";
					break;
					case 2:
					$strReturn = "doce";
					break;
					case 3:
					$strReturn = "trece";
					break;
					case 4:
					$strReturn = "catorce";
					break;
					case 5:
					$strReturn = "quince";
					break;
				}
			}
			elseif( $arrThis["key"] == 1 && $arrThis["value"] == 2 && $arrParts["int"][0] == 0 ) {
				$strReturn = "veinte";
			}
			elseif ($arrThis["key"]==2 && $arrThis["value"]==1 && $arrParts["int"][1] == 0 && $arrParts["int"][0] == 0) {
				$strReturn = "cien";
			}
			else {
				$strTMP = (isset($arrStrings[$arrThis["key"]][$arrThis["value"]]))?$arrStrings[$arrThis["key"]][$arrThis["value"]]:"";
			}

			if (empty($strReturn)) {
				$strTMP = str_replace(" y ","",$strTMP);
			}
			$strReturn = $strTMP.$strReturn;
		}
	}
	/*while ( ($arrThis = each($arrParts["int"]) ) && $arrThis["key"] < 3) {
		
	}*/

	$strMiles = "";
	$strMillones = "";
	for ($i = 3;$i < count($arrParts["int"]);$i++) {
		if ($i<6) {
			$strMiles = $arrParts["int"][$i].$strMiles;
		}
		else {
			$strMillones = $arrParts["int"][$i].$strMillones;
		}
	}

	if (!empty($strMiles)) {
		if ($strMiles == 1) {
			$strReturn = "un mil ".$strReturn;
		}
		else if ($strMiles > 0) {
			$strReturn = GetNumeroEnLetras($strMiles, $arrStrings)." mil ".$strReturn;
		}
	}

	if (!empty($strMillones)) {
		if ($strMillones == 1) {
			$strReturn = "un mill�n ".$strReturn;
		}
		else if ($strMillones > 0) {
			$strReturn = GetNumeroEnLetras($strMillones, $arrStrings)." millones ".$strReturn;
		}
	}

	//Agrego este proceso para sustituir las palabras con tilde(solo son los numeros 16,22,23,26)
	$strReturn = str_replace(array(
			"dieciseis","veintidos","veintitres","veintiseis"
		),array(
			"diecis�is","veintid�s","veintitr�s","veintis�is"
		),$strReturn);
	return trim($strReturn);
}

///TIPOS
//// 1 = FACTURA JANAMA
//// 2 = FACTURA BRAINERD
//// 3 = RECIBO DE COMPRA
//// 4 = FACTURA DE VENTA JANAMA
//// 5 = FACTURA DE VENTA BRAINERD
//// 6 = FACTURA DE EXPOTACI?n BRAINERD

$intTipo = 0;
$intFactura = 0; 
$strMd5=isset($_REQUEST['UUID'])?trim($_REQUEST['UUID']):(isset($_REQUEST['uuid'])?trim($_REQUEST['uuid']):'');
$strQuery = "SELECT factura.factura,
					sociedad.codigo_pos as codigo_soci,
					tipo_transaccion.codigo_pos as codigo_transac
			FROM factura 
			INNER JOIN serie	
				ON factura.serie = serie.serie 
			INNER JOIN sociedad 
				ON serie.sociedad = sociedad.sociedad
			INNER JOIN tipo_transaccion
				ON serie.tipo_transaccion = tipo_transaccion.tipo_transaccion 
				WHERE CONVERT(NVARCHAR(32),HashBytes('MD5', CONVERT(varchar(50),factura)),2) = UPPER('{$strMd5}')";
$arrFacInfo = sqlGetValueFromKey($strQuery,false,true);
$intFactura = isset($arrFacInfo['factura'])?intval($arrFacInfo['factura']):0;
$strSociedad = isset($arrFacInfo['codigo_soci'])?trim(strtoupper($arrFacInfo['codigo_soci'])):'';
$strTipoTransac = isset($arrFacInfo['codigo_transac'])?trim(strtoupper($arrFacInfo['codigo_transac'])):'';
//print_r($arrFacInfo);
if ( $strSociedad == 'JANAMA' && ( $strTipoTransac == 'PAYLOAN'   )  ){
    $intTipo = 1;
    //$strTitulo = "FACTURA JANAMA";
}
else if ( $strSociedad == 'BRAINERD' && ( $strTipoTransac == 'PAYLOAN')  ){
    $intTipo = 2;
}
else if ( $strSociedad == 'BRAINERD' && $strTipoTransac == 'PURCH' ){
    $intTipo = 3;
}
else if ( $strSociedad == 'JANAMA' && $strTipoTransac == 'DLOAN' ){
    $intTipo = 4;
}
else if ( $strSociedad == 'BRAINERD' && ( $strTipoTransac == 'SLE' || $strTipoTransac == 'LAY' || $strTipoTransac == 'DLAY' )  ){
    $intTipo = 5;
}
else if ( $strSociedad == 'BRAINERD' && $strTipoTransac == 'EXP' ){
    $intTipo = 6;
}

fntGeneracionPDF($intTipo, $intFactura,false);
?>