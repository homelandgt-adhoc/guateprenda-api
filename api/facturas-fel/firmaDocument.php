<?php 
require_once("../core/main.php");
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$data = json_decode(file_get_contents("php://input"));
if( !empty($data->codigo) && !empty($data->llave) && !empty($data->archivo) && !empty($data->alias) ){
    $arrReturn = array();
    $arrReturn['descripcion'] = 'Documento firmado con éxito';
    $arrReturn['resultado'] = 'true';
    $arrReturn['archivo'] = $data->archivo;
    http_response_code(200);
    print json_encode($arrReturn);
    /*if($product->create()){
        http_response_code(201);
        echo json_encode(array("message" => "Product was created."));
    }
    else{
        http_response_code(503);
        echo json_encode(array("message" => "No fue posible firmar el documento."));
    }*/
}
else{
  
    http_response_code(400);
    echo json_encode(array("message" => "No fue posible firmar el documento. Datos incompletos."));
}
?>