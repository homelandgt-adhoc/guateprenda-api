<?php 
require_once("../core/main.php");
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
$data = json_decode(file_get_contents("php://input"));
if( !empty($data->xml_dte) && !empty($data->nit_emisor) && !empty($data->correo_copia) ){
    $strXmlDte = base64_decode($data->xml_dte); 
    $strXmlDte = str_replace('<dte:','<',$strXmlDte);
    $strXmlDte = str_replace('</dte:','</',$strXmlDte);
    $strXmlDte = str_replace('<cno:ReferenciasNota','<ReferenciasNota',$strXmlDte);
    
    $XmlDte = simplexml_load_string($strXmlDte);
    $json = json_encode($XmlDte);
    $arrDte = json_decode($json,TRUE);
    
    $arrInfoFactura = array('dt'=>array(),'emisor'=>array(),'receptor'=>array(),'productos'=>array());
    
    //print_r($arrDte);
    $arrInfoFactura['dt'] = $arrDte['SAT']['DTE']['DatosEmision']['DatosGenerales']['@attributes'];    
    $arrInfoFactura['emisor'] = $arrDte['SAT']['DTE']['DatosEmision']['Emisor']['@attributes'];    
    $arrInfoFactura['emisor']['DireccionEmisor'] = $arrDte['SAT']['DTE']['DatosEmision']['Emisor']['DireccionEmisor'];    
    
    $arrInfoFactura['receptor'] = $arrDte['SAT']['DTE']['DatosEmision']['Receptor']['@attributes'];    
    $arrInfoFactura['receptor']['DireccionReceptor'] = $arrDte['SAT']['DTE']['DatosEmision']['Receptor']['DireccionReceptor'];    
    
    $arrItems = isset($arrDte['SAT']['DTE']['DatosEmision']['Items'])?$arrDte['SAT']['DTE']['DatosEmision']['Items']:array();
    $arrTotales = isset($arrDte['SAT']['DTE']['DatosEmision']['Totales'])?$arrDte['SAT']['DTE']['DatosEmision']['Totales']:array();
    $arrComplementos = isset($arrDte['SAT']['DTE']['DatosEmision']['Complementos'])?$arrDte['SAT']['DTE']['DatosEmision']['Complementos']:array();
    
    $intCorr = 0;
    
    if(  isset($arrItems['Item']['@attributes']) ){
        $intCorr++;
        $arrInfoFactura['productos'][$intCorr] = $arrItems['Item'];
    }
    else{
        $arrProdSearch = $arrItems['Item'];    
        foreach( $arrProdSearch as $keyItems => $valueItems ){
            $intCorr++;
            $arrInfoFactura['productos'][$intCorr] = $valueItems;    
        }
    }
    
    $arrAdendas = isset($arrDte['SAT']['Adenda'])?$arrDte['SAT']['Adenda']:array();         
    $strTiendaCod = '';
    $strSociedadCod = '';
    $strUsuarioTransc = '';
    $strTipoTransCod = '';
    $strContrato = '';
    
    foreach($arrAdendas as $key => $value){
        $strKey = strtoupper($key);
        if( $strKey == 'USUARIO' ){
            $strUsuarioTransc = $value;
        }
        else if( $strKey == 'SOCIEDAD' ){
            $strSociedadCod = $value;
        }
        else if( $strKey == 'TIENDA' ){
            $strTiendaCod = $value;
        }
        else if( $strKey == 'TIPO' ){
            $strTipoTransacCod = $value;
        }
        else if( $strKey == 'PRESTAMO' ){
            $strContrato = $value;
        }
        
        //print $key.'-'.$value;            
    } 
    
    if( $strTipoTransacCod != 'VOID' ){
        $strQuery = "SELECT serie.serie as id_serie,
                            t.activo   tienda_activa,
                            soci.activo sociedad_activa,
                            tt.activo tipotransac_activa,
                            serie.cai,
                            serie.correlativo_inicial,
                            serie.correlativo_final,                        
                            serie.fecha_expiracion,
                            serie.corr_primer_num,
                            serie.corr_segundo_num,
                            serie.corr_tercer_num,
                            CASE WHEN FORMAT(fecha_expiracion,'yyyy-MM-dd') >= FORMAT(GETDATE(),'yyyy-MM-dd') THEN 1
                                ELSE 0 
                            END 
                            as valid_fecha                                            
                    FROM dbo.serie 
                    INNER JOIN dbo.tienda t
                        ON serie.tienda = t.tienda 
                    INNER JOIN dbo.sociedad soci
                        ON serie.sociedad = soci.sociedad
                    INNER JOIN dbo.tipo_transaccion tt
                        ON serie.tipo_transaccion = tt.tipo_transaccion
                    WHERE serie.activo = 1
                    AND t.codigo_pos = '{$strTiendaCod}'
                    AND soci.codigo_pos = '{$strSociedadCod}'
                    AND tt.codigo_pos = '{$strTipoTransacCod}'";
        $arrSerie = sqlGetValueFromKey($strQuery,false,true);
        $boolError = false;
        $arrReturn = array();
        $arrReturn['descripcion'] = '';
        $arrReturn['resultado'] = 'false';
        if( count($arrSerie) > 0 ){
            $intTiendaActiva = isset($arrSerie['tienda_activa'])?intval($arrSerie['tienda_activa']):0;
            $intSociedadActiva = isset($arrSerie['sociedad_activa'])?intval($arrSerie['sociedad_activa']):0;
            $intTipoTransacActiva = isset($arrSerie['tipotransac_activa'])?intval($arrSerie['tipotransac_activa']):0;
            if( $intTiendaActiva == 1 && $intSociedadActiva == 1 && $intTipoTransacActiva == 1 ){            
                $intFechaValidaSerie = isset($arrSerie['valid_fecha'])?intval($arrSerie['valid_fecha']):0;
                if( $intFechaValidaSerie == 1){
                    $strNitReceptor = isset($arrInfoFactura['receptor']['IDReceptor'])?trim(strtoupper($arrInfoFactura['receptor']['IDReceptor'])):'CF';
                    $strNombreReceptor = isset($arrInfoFactura['receptor']['NombreReceptor'])?utf8_decode(trim($arrInfoFactura['receptor']['NombreReceptor'])):'--CF--';
                    $strDireccionReceptor = isset($arrInfoFactura['receptor']['DireccionReceptor']['Direccion'])?utf8_decode(trim($arrInfoFactura['receptor']['DireccionReceptor']['Direccion'])):''; 
                    $strCorreoReceptor = isset($arrInfoFactura['receptor']['CorreoReceptor'])?trim($arrInfoFactura['receptor']['CorreoReceptor']):'';
                    
                    $sinISVTotal  = isset($arrTotales['TotalImpuestos']['TotalImpuesto']['@attributes']['TotalMontoImpuesto'])?floatval($arrTotales['TotalImpuestos']['TotalImpuesto']['@attributes']['TotalMontoImpuesto']):0;
                    $intCliente = 0;
                    if( $strNitReceptor != 'CF' ){                 
                        $arrInfoNit = sqlGetValueFromKey("SELECT cliente FROM cliente WHERE nit = '{$strNitReceptor}'");
                        $intCliente = isset($arrInfoNit['cliente'])?intval($arrInfoNit['cliente']):0;
                        if( $intCliente == 0){
                            $strQuery = "INSERT INTO cliente(NOMBRE,NIT,DIRECCION,CORREO,ACTIVO, ADD_USER,ADD_FECHA)VALUES ('{$strNombreReceptor}','{$strNitReceptor}','{$strDireccionReceptor}','{$strCorreoReceptor}',1,1,GETDATE())";
                            db_query($strQuery); 
                            $intCliente = intval(sqlGetValueFromKey("SELECT cliente FROM cliente WHERE nit = '{$strNitReceptor}'"));

                            //print $intCliente; 
                            //die();
                        }
                        else{
                            $strQuery = "UPDATE cliente 
                                        SET NOMBRE = '{$strNombreReceptor}',
                                            DIRECCION = '{$strDireccionReceptor}',
                                            CORREO = '{$strCorreoReceptor}',
                                            ACTIVO = 1, 
                                            MOD_USER = 1,
                                            MOD_FECHA = GETDATE()
                                        WHERE cliente = {$intCliente}";
                            db_query($strQuery); 
                            
                        }
                    }
                    $intCorrelativoSerie = 0;
                    if($intCliente == 0){
                        $strNombreFac = "'{$strNombreReceptor}'";
                        $strDireccionFac = "'{$strDireccionReceptor}'";
                        
                    }
                    else{
                        $strNombreFac = "NULL";
                        $strDireccionFac = "NULL";
                    }
                    $strXmlFac = $strXmlDte;
                    $intIdSerie = isset($arrSerie['id_serie'])?intval($arrSerie['id_serie']):0;
                    $strFechaEmision= '';
                    $strFechaFac = isset($arrInfoFactura['dt']['FechaHoraEmision'])?trim($arrInfoFactura['dt']['FechaHoraEmision']):'';
                    if( $strFechaFac != ''){
                        $strFechaEmision = $strFechaFac;
                        $arrExplodeFec = explode('T',$strFechaFac);
                        $arrExplodeFec2 = isset($arrExplodeFec[1])? explode('-',$arrExplodeFec[1]) :array();   
                        $strDateFac = isset($arrExplodeFec[0])?$arrExplodeFec[0]:'';
                        $strTimeFac = isset($arrExplodeFec2[0])?$arrExplodeFec2[0]:'';
                        if( $strDateFac != '' &&  $strTimeFac != ''){
                            $strFechaFac = $strDateFac.' '.$strTimeFac;        
                        }
                        
                    }
                    
                    $arrInfoSerie = (sqlGetValueFromKey("SELECT MAX(correlativo_serie) as corre_serie FROM factura WHERE serie = {$intIdSerie}")); 
                    $intCorreFacMax = isset($arrInfoSerie['corre_serie'])? intval($arrInfoSerie['corre_serie']):0;
                    $intCorreMin = isset($arrSerie['correlativo_inicial'])?intval($arrSerie['correlativo_inicial']):0;
                    $intCorreMax = isset($arrSerie['correlativo_final'])?intval($arrSerie['correlativo_final']):0;
                    
                    if( $intCorreFacMax == 0){
                        $intCorreFacMax = $intCorreMin;
                    }
                    else{
                        if( $intCorreMin > $intCorreFacMax ){
                            $intCorreFacMax = $intCorreMin;
                        }
                        else{
                            $intCorreFacMax++;
                        }                        
                        
                    }

                    if( $intCorreFacMax <= $intCorreMax ){
                        $strFechaFac = ($strFechaFac != '')?"'{$strFechaFac}'":"GETDATE()";
                        $strContrato = ($strContrato != '')?"'{$strContrato}'":'NULL';                    
                        $intCliente = ($intCliente > 0)?"{$intCliente}":'NULL';
                        
                        $intFactura = 0;
                        $intFacAnterior = sqlGetValueFromKey("SELECT MAX(factura) as factura FROM factura");
                        
                        $strQuery = "INSERT INTO factura (correlativo_serie, cliente, nombre, direccion, xml, serie, fecha, usuario_transaccion, add_user, add_fecha,contrato,isv) 
                                    VALUES ({$intCorreFacMax}, {$intCliente}, {$strNombreFac}, {$strDireccionFac}, '{$data->xml_dte}', {$intIdSerie}, {$strFechaFac}, '{$strUsuarioTransc}', 1, GETDATE(), {$strContrato}, {$sinISVTotal}) ";
                        db_query($strQuery); 
                        /*
                        global $arrConfigSite; 
                        
                        print $intIdSerie;
                        $params = array(
                                $intCliente,
                                $strNombreFac, 
                                $strDireccionFac, 
                                $data->xml_dte, 
                                Array(&$intIdSerie, SQLSRV_PARAM_IN, SQLSRV_PHPTYPE_VARCHAR, SQLSRV_PHPTYPE_INT)
                        );*/
                        //$params = array($intCorreFacMax);
                        //$sql = "EXEC SP_INSERT_FACTURA ?, '?','?','?', ? ";
                        //$sql = "EXEC SP_INSERT_FACTURA '?'  ";
                        
                        //$stmt = sqlsrv_query($arrConfigSite["db"]["database_resource"] , $sql, $params);
                        /*if ($stmt === false) {
                            $message = '';
                            if (($errors = sqlsrv_errors()) != null) {
                                foreach ($errors as $error) {
                                    $message .= "SQLSTATE: " . $error['SQLSTATE'] . ", ";
                                    $message .= "code: " . $error['code'] . ", ";
                                    $message .= "message: " . $error['message'];
                                }
                            }
                            $arrData["estado"] = "error";
                            $arrData["mensaje"] = $message;
                        } else {
                            while ($row = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC)) {
                                $row = array_map("utf8_encode", $row);
                                $row = array_map("trim", $row);
                                $arrData["data"] = $row;
                            }
                        }
                        sqlsrv_free_stmt($stmt);
                        */
                        //print_r($arrData);
                        
                        $intFactura = db_insert_id();
                        if( $intFactura == $intFacAnterior )
                            $intFactura = 0;        
                        
                        //$intFactura = 0;
                        if( $intFactura > 0){
                            if( isset($arrInfoFactura['productos']) && is_array($arrInfoFactura['productos']) ){
                                foreach($arrInfoFactura['productos'] as $keyP => $valueP){
                                    $strDescripP = isset($valueP['Descripcion'])?utf8_decode(trim($valueP['Descripcion'])):'';
                                    $intCantidad = isset($valueP['Cantidad'])?intval($valueP['Cantidad']):0;
                                    $sinPrecioUni = isset($valueP['PrecioUnitario'])?floatval($valueP['PrecioUnitario']):0;
                                    $sinSubtotal = isset($valueP['Total'])?floatval($valueP['Total']):0;
                                    $sinISV = isset($valueP['Impuestos']['Impuesto']['MontoImpuesto'])?floatval($valueP['Impuestos']['Impuesto']['MontoImpuesto']):0;
                                    $strQuery = "INSERT INTO factura_detalle 
                                                    ( factura, descripcion, cantidad, precio_unitario, subtotal, isv, add_user, add_fecha) 
                                                    VALUES ({$intFactura}, '{$strDescripP}', {$intCantidad}, {$sinPrecioUni}, {$sinSubtotal}, {$sinISV}, '1', GETDATE() )";
                                    db_query($strQuery);
                                }    
                            }
                                
                            $arrReturn['descripcion'] = 'Validado y Certificado Exitosamente';
                            $arrReturn['descripcion_alertas_sat'] = array();
                            $arrReturn['resultado'] = 'true';
                            
                            $intCorre1 = isset($arrSerie['corr_primer_num'])?intval($arrSerie['corr_primer_num']):0;
                            $intCorre2 = isset($arrSerie['corr_segundo_num'])?intval($arrSerie['corr_segundo_num']):0;
                            $intCorre3 = isset($arrSerie['corr_tercer_num'])?intval($arrSerie['corr_tercer_num']):0;
                            
                            $strCorre1 = sprintf('%03d', $intCorre1);
                            $strCorre2 = sprintf('%03d', $intCorre2);
                            $strCorre3 = sprintf('%02d', $intCorre3);
                            $strCorre4 = sprintf('%08d', $intCorreFacMax);
                            $arrReturn['serie'] = "{$strCorre1}-{$strCorre2}-{$strCorre3}-{$strCorre4}";
                            
                            $arrReturn['numero'] = 123456789;
                            $arrReturn['alertas_infile'] = true;
                            $arrReturn['origen'] = "Validaciones XML Segun FEL Reglas y Validaciones Versión 1.4 | Junio 2020 | Acuerdo de Directorio SAT 13-2018";
                            
                            $strFacturaMd5 = md5($intFactura);
                            $arrReturn['uuid'] = $strFacturaMd5;
                            $arrReturn['informacion_adicional'] = "transaccion: {$strFacturaMd5}, sysid: {$strFacturaMd5}, etag: 1fa97f4b670c9dffc9b34bffcd5c4212, Nodo: 2f90bff8-e1e9-47c9-9823-86f493edaee4";
                            $arrReturn['fecha'] = $strFechaEmision;     
                        }
                        else{
                            $boolError = true;    
                            $arrReturn['descripcion'] = 'No se creo la factura.';        
                        }    
                    }
                    else{
                        $boolError = true;    
                        $arrReturn['descripcion'] = 'Correlativo m�ximo excedido.'; 
                    }
                    
                    
                    //print $intCliente.' cliente ID';
                }
                else{
                    $boolError = true;    
                    $arrReturn['descripcion'] = 'Fecha de expiraci�n.';    
                }
            }
            else{
                $boolError = true; 
                if( $intTiendaActiva == 0 ){
                    $arrReturn['descripcion'] = 'No se encuentra activa la tienda.';
                }   
                else if($intTiendaActiva == 0  ){
                    $arrReturn['descripcion'] = 'No se encuentra activa la sociedad.';
                } 
                else if( $intTiendaActiva == 0  ){
                    $arrReturn['descripcion'] = 'No se encuentra activa el tipo de transacci�n.';
                }
                else{
                    $arrReturn['descripcion'] = 'Error 404.';
                }
            }
        }
        else{
            $boolError = true;    
            $arrReturn['descripcion'] = 'No se cuenta con una serie activa configurada para transacci�n';
        
        }    
    } 
    else{
        $intFactura = 0; 
        $strMd5UUid = isset($arrComplementos['Complemento']['ReferenciasNota']['@attributes']['NumeroAutorizacionDocumentoOrigen'])?trim($arrComplementos['Complemento']['ReferenciasNota']['@attributes']['NumeroAutorizacionDocumentoOrigen']):'';
        $strMotivoAnulacion = isset($arrComplementos['Complemento']['ReferenciasNota']['@attributes']['MotivoAjuste'])? substr(trim($arrComplementos['Complemento']['ReferenciasNota']['@attributes']['MotivoAjuste']),0,250) :'';
        $strQuery = "SELECT factura.factura,
                            sociedad.codigo_pos as codigo_soci,
                            tipo_transaccion.codigo_pos as codigo_transac,
                            factura.correlativo_serie,
                            serie.corr_primer_num,
                            serie.corr_segundo_num,
                            serie.corr_tercer_num
                            
                    FROM factura 
                    INNER JOIN serie	
                        ON factura.serie = serie.serie 
                    INNER JOIN sociedad 
                        ON serie.sociedad = sociedad.sociedad
                    INNER JOIN tipo_transaccion
                        ON serie.tipo_transaccion = tipo_transaccion.tipo_transaccion 
                        WHERE CONVERT(NVARCHAR(32),HashBytes('MD5', CONVERT(varchar(50),factura)),2) = UPPER('{$strMd5UUid}')";
        $arrFacInfo = sqlGetValueFromKey($strQuery,false,true);
        $intFactura = isset($arrFacInfo['factura'])?intval($arrFacInfo['factura']):0;
        
        if( $intFactura > 0 ){
            $strQuery = "UPDATE factura 
                                SET anulada = 1,
                                    usuario_anulacion_api = '{$strUsuarioTransc}',
                                    motivo_anulacion = '{$strMotivoAnulacion}',
                                    fecha_anulacion = GETDATE()
                                WHERE factura = {$intFactura}";
            db_query($strQuery);
            

            $arrReturn['descripcion'] = 'Validado y Certificado Exitosamente';
            $arrReturn['descripcion_alertas_sat'] = array();
            $arrReturn['resultado'] = 'true';
            
            $intCorre1 = isset($arrFacInfo['corr_primer_num'])?intval($arrFacInfo['corr_primer_num']):0;
            $intCorre2 = isset($arrFacInfo['corr_segundo_num'])?intval($arrFacInfo['corr_segundo_num']):0;
            $intCorre3 = isset($arrFacInfo['corr_tercer_num'])?intval($arrFacInfo['corr_tercer_num']):0;
            $intSerieFac = isset($arrFacInfo['correlativo_serie'])?intval($arrFacInfo['correlativo_serie']):0;
            
            $strCorre1 = sprintf('%03d', $intCorre1);
            $strCorre2 = sprintf('%03d', $intCorre2);
            $strCorre3 = sprintf('%02d', $intCorre3);
            $strCorre4 = sprintf('%08d', $intSerieFac);
            $arrReturn['serie'] = "{$strCorre1}-{$strCorre2}-{$strCorre3}-{$strCorre4}";
            
            $arrReturn['numero'] = 123456789;
            $arrReturn['alertas_infile'] = true;
            $arrReturn['origen'] = "Validaciones XML Segun FEL Reglas y Validaciones Versión 1.4 | Junio 2020 | Acuerdo de Directorio SAT 13-2018";
            
            $strFacturaMd5 = md5($intFactura);
            $arrReturn['uuid'] = $strFacturaMd5;
            $arrReturn['informacion_adicional'] = "transaccion: {$strFacturaMd5}, sysid: {$strFacturaMd5}, etag: 1fa97f4b670c9dffc9b34bffcd5c4212, Nodo: 2f90bff8-e1e9-47c9-9823-86f493edaee4";
        }
        else{
            $boolError = true;    
            $arrReturn['descripcion'] = 'No existe dicha factura';
        }
        


    }                    
    
    
    if( !$boolError ){
        $arrReturn['xml_certificado'] = $data->xml_dte;   
        http_response_code(200);            
    }
    else{
        http_response_code(400);        
    }
    
    echo json_encode($arrReturn);

}
else{
  
    http_response_code(400);
    echo json_encode(array("message" => "No fue posible firmar el documento. Datos incompletos."));
}
?>