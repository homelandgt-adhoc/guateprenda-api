<?php 
require_once("../core/main.php");
function fntGeneracionPDF($intTipo, $intFactura = 0, $boolPrueba = false){
        
    $strTexto = "";
    if ( $intTipo == 1 ){
        $strTexto = fntDrawFacturaJanama();
    }
    else if ( $intTipo == 2 ){
        $strTexto = fntDrawFacturaBrainerd();
    }
    else if ( $intTipo == 3 ){
        $strTexto = fntDrawReciboCompra();
    }
    else if ( $intTipo == 4 ){
        $strTexto = fntDrawFacturaVentaJanama();
    }
    else if ( $intTipo == 5 ){
        $strTexto = fntDrawFacturaVentaBrainerd();
    }
    else if ( $intTipo == 6 ){
        $strTexto = fntDrawFacturaExportacionBrainerd();
    }
    
    fntImpresionPDF($intTipo, $strTexto);
    
}

function fntDrawFacturaJanama(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">JANAMA HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Cliente: XXXXXXXXXXXXXXX</td>
                                      <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RTN / ID: 999999999000000</td>
                                      <td style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 101010101010101</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">INTERESES</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 51.87</td>
                                      <td style=\"text-align: left;\">L. 51.87</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RECARGO</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 3.50</td>
                                      <td style=\"text-align: left;\">L. 3.50</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exenta</td>
                                      <td style=\"text-align: right;\">L. 55.37</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exonerada</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Sub Total</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 55.37</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Cincuenta y cinco Lempiras con 37/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
                                      <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntDrawFacturaBrainerd(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Cliente: XXXXXXXXXXXXXXX</td>
                                      <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RTN / ID: 999999999000000</td>
                                      <td style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 101010101010101</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">INTERESES</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 51.87</td>
                                      <td style=\"text-align: left;\">L. 51.87</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RECARGO</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 3.50</td>
                                      <td style=\"text-align: left;\">L. 3.50</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exenta</td>
                                      <td style=\"text-align: right;\">L. 55.37</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exonerada</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Sub Total</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 55.37</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Cincuenta y cinco Lempiras con 37/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
                                      <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntDrawReciboCompra(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Proveedor: XXXXXXXXXXXXXXX</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Domicilio del proveedor: XXXXXXXXXXXXXXX</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Telefono: 3366-8888</td>
                                  </tr>
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">RTN / ID: 999999999000000</td>
                                      <td width=\"50%\" style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 101010101010101</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">CEPILLO ELECTRICO DE PELO</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 450.00</td>
                                      <td style=\"text-align: left;\">L. 450.00</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; \">ARTICULOS USADOS</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 450.00</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Cuatrocientos cincuenta Lempiras con 00/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"40%\" style=\"text-align: left;\">Original: Contribuyente adquiriente</td>
                                      <td width=\"60%\" style=\"text-align: left;\">Copia: Proveedor / Contabilidad</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Declaro que soy el propietario de los bienes descritos en este documento, por lo cual eximo a la Brainerd Honduras S.A. de toda responsabilidad.</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">__________________________________________</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">FIRMA DEL PROVEEDOR</td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntDrawFacturaVentaJanama(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">LA FACTURA ES BENEFICIO DE TODOS EXIJALA</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">JANAMA HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Cliente: BRAINERD HONDURAS S.A. DE C.V.</td>
                                      <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RTN / ID: 08019016853521</td>
                                      <td style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 00001000000422</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">00001000000422 - JOYAS</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 1,300.20</td>
                                      <td style=\"text-align: left;\">L. 1,300.20</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left;\">VENTA DE ARTICULOS USUADOS</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exenta</td>
                                      <td style=\"text-align: right;\">L. 1,300.20</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exonerada</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Sub Total</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 1,300.20</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Un mil trescientos Lempiras con 20/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
                                      <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntDrawFacturaVentaBrainerd(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Cliente: XXXXXXXXXXXXXXX</td>
                                      <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RTN / ID: 999999999000000</td>
                                      <td style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 101010101010101</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">CEPILLO ELECTRICO DE PELO</td>
                                      <td style=\"text-align: left;\">1</td>
                                      <td style=\"text-align: left;\">L. 450.00</td>
                                      <td style=\"text-align: left;\">L. 450.00</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left;\">ARTICULOS USADOS</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exenta</td>
                                      <td style=\"text-align: right;\">L. 450.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exonerada</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Sub Total</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 450.00</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Cuatrocientos cincuenta Lempiras con 00/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
                                      <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntDrawFacturaExportacionBrainerd(){
    
    $strTexto = "";
    
    $strTexto .= "<table width=\"100%\">
                      <tr>
                          <td style=\"text-align: center;\">BRAINERD HONDURAS S.A. DE C.V.</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">RTN &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 080019016854560</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Blvd. Moraz�n Edif. Centro Moraz�n, Nivel 9 Tegucigalpa, Honduras</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2221-2929, e mail: info.brainerd@gmail.com</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Barrio El Centro 1 Calle 0 Ave. S-O, Siguatepeque, Comayagua</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: center;\">Tel 2273-7181 / 9696-3250</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Factura: 000-004-01-00000111</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">CAI: 4D4D4-B9B9B9-C3C3C3-U7U7U7-Q1Q1Q1-90</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Rango autorizado: 000-001-01-00000101 al 000-001-01-00000501</td>
                      </tr>
                      <tr>
                          <td style=\"text-align: left;\">Fecha limite de Emisi�n: 11-12-2019</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Cliente: XXXXXXXXXXXXXXX</td>
                                      <td width=\"50%\" style=\"text-align: right;\">G=GRAVADA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">RTN / ID: 999999999000000</td>
                                      <td style=\"text-align: left;\">Fecha: DD/MM/AAAA</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Contrato: 101010101010101</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. O/C Exenta</td>
                                      <td style=\"text-align: left;\">No Registo de la SAG:</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">No. Registro Exonerado:</td>
                                      <td style=\"text-align: left;\">&nbsp;</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Tasa de Cambio: 24.57</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Valor en US$: 411.07</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"45%\" style=\"text-align: left; border-bottom: 1px solid black;\">Descripci�n</td>
                                      <td width=\"15%\" style=\"text-align: left; border-bottom: 1px solid black;\">Cant.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">P. Unit.</td>
                                      <td width=\"20%\" style=\"text-align: left; border-bottom: 1px solid black;\">Sub Total</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Kg 10K</td>
                                      <td style=\"text-align: left;\">1.95</td>
                                      <td style=\"text-align: left;\">L. 2,000.00</td>
                                      <td style=\"text-align: left;\">L. 3,900.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Kg 14K</td>
                                      <td style=\"text-align: left;\">0.90</td>
                                      <td style=\"text-align: left;\">L. 2,000.00</td>
                                      <td style=\"text-align: left;\">L. 1,800.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Kg 18K</td>
                                      <td style=\"text-align: left;\">1.50</td>
                                      <td style=\"text-align: left;\">L. 2,000.00</td>
                                      <td style=\"text-align: left;\">L. 3,000.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Kg 24K</td>
                                      <td style=\"text-align: left;\">0.70</td>
                                      <td style=\"text-align: left;\">L. 2,000.00</td>
                                      <td style=\"text-align: left;\">L. 1,500.00</td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"4\" style=\"text-align: left; border-bottom: 1px solid black;\">&nbsp;</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"50%\" style=\"text-align: left;\">Descuento / Rebaja</td>
                                      <td width=\"50%\" style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Gravada 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exenta</td>
                                      <td style=\"text-align: right;\">L. 10,100.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Venta Exonerada</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Sub Total</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 15%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">ISV 18%</td>
                                      <td style=\"text-align: right;\">L. 0.00</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\"><b>TOTAL</b></td>
                                      <td style=\"text-align: right;\"><b>L. 10,100.00</b></td>
                                  </tr>
                                  <tr>
                                      <td colspan=\"2\" style=\"text-align: left;\">Son: Diez mil cien Lempiras con 00/100</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td style=\"border-bottom: 1px dashed black;\">&nbsp;</td>
                      </tr>
                      <tr>
                          <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                              <table width=\"100%\">
                                  <tr>
                                      <td width=\"30%\" style=\"text-align: left;\">Original: Cliente</td>
                                      <td width=\"70%\" style=\"text-align: left;\">Copia: Obligador tributario emisor</td>
                                  </tr>
                                  <tr>
                                      <td style=\"text-align: left;\">Atendido por:</td>
                                      <td style=\"text-align: left;\">NOMBRE DE EMPLEADO</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>";
    
    return $strTexto;
    
}

function fntImpresionPDF($intTipo, $strTexto = ""){
    $intTipo = intval($intTipo);
    $strTexto = !empty($strTexto) ? $strTexto : "";
    
    if ( $intTipo && !empty($strTexto) ){
        
        $strTitulo = "";
        if ( $intTipo == 1 ){
            $strTitulo = "FACTURA JANAMA";
        }
        else if ( $intTipo == 2 ){
            $strTitulo = "FACTURA BRAINERD";
        }
        else if ( $intTipo == 3 ){
            $strTitulo = "RECIBO DE COMPRA";
        }
        else if ( $intTipo == 4 ){
            $strTitulo = "FACTURA DE VENTA JANAMA";
        }
        else if ( $intTipo == 5 ){
            $strTitulo = "FACTURA DE VENTA BRAINERD";
        }
        else if ( $intTipo == 6 ){
            $strTitulo = "FACTURA DE EXPORTACI�N BRAINERD";
        }
        
        
        require_once("../core/tcpdf/tcpdf.php");
        
        $pdf = new TCPDF("P", "mm", "LETTER", false, 'ISO-8859-1', false);

        $pdf->SetTitle(utf8_encode($strTitulo));

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        $pdf->SetFont('arial', '', 8);
        
        $pdf->AddPage();
        
        $pdf->writeHTML($strTexto, true, true, true, true, 'C');
        
        $pdf->Output();
        
    }
    
}

///TIPOS
//// 1 = FACTURA JANAMA
//// 2 = FACTURA BRAINERD
//// 3 = RECIBO DE COMPRA
//// 4 = FACTURA DE VENTA JANAMA
//// 5 = FACTURA DE VENTA BRAINERD
//// 6 = FACTURA DE EXPOTACI�N BRAINERD

$intTipo = 6;
$intFactura = 0; 
$boolPrueba = true;

fntGeneracionPDF($intTipo, $intFactura, $boolPrueba);

?>